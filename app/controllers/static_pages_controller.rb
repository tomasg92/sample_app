class StaticPagesController < ApplicationController
  def home # This is the one that generates views
  end

  def help
  end

  def about    
  end

  def contact
  end
end
